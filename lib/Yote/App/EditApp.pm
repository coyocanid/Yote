package Yote::App::EditApp;

use strict;
use warnings;

use base 'Yote::App';

sub _init {
    my $self = shift;
    $self->SUPER::_init;
} #_init

sub _load {
    my $self = shift;
    $self->SUPER::_load;
    $self->get_sets( {
	
		     } );
} #_load


1;
