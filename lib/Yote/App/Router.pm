package Yote::App::Router;

use strict;
use warnings;

use Apache2::RequestRec;
use Apache2::Const;
use APR::Finfo;
use APR::Const qw(FINFO_NORM);

sub handler {
    print STDERR "HANDLA\n";
    my( $r ) = shift;
    my $filename = $r->filename;
    my $path = $r->path_info;
    my $newfile = "/var/www/html/test.html";
    $r->filename( $newfile );
    $r->finfo(APR::Finfo::stat($newfile, APR::Const::FINFO_NORM, $r->pool));
    print STDERR Data::Dumper->Dump([$r,$path,$filename,$r->uri,"ROUTER"]);
    return Apache2::Const::OK;    
}

1;

__END__
