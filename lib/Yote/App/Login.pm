package Yote::App::Login;

#
# A login holds credentials and app
# accounts.
#

use strict;
use warnings;

use Yote::Obj;
use base 'Yote::Obj';

sub _load {
    my $self = shift;
    $self->get__app_accounts( {} );
    $self->allow_methods( qw ( update_password ) );
    $self->disallow_methods( qw ( needs_password_reset is_admin ) );
}

sub crypt_password {
    my( $self, $password ) = @_;
    return crypt( $password, length( $password ) . Digest::MD5::md5_hex($self->get_login_name) );
}

sub update_password {
    my ($self, $args) = @_;
    my ($old_pw, $new_pw) = @$args{qw(old_password new_password)};
    my $old_c = $self->crypt_password( $old_pw );
    if ($old_c ne $self->get__enc_pw) {
        return 0, "Update password failed : incorrect old password";
    }
    if (length($new_pw) < 4) {
        return 0, "Update password failed : password too short";
    }
    my $new_c = $self->crypt_password( $new_pw );
    $self->set__enc_pw( $new_c );
    $self->set_needs_password_reset(undef);
    return 1, 'password updated';
}  #update_password



#
# one login with credentials, multiple app accounts
# each app may require its own credentials
#

1;

__END__

=head1 NAME

Yote::App::Login - 

=head1 SYNOPSIS


=head1 DESCRIPTION

=head1 METHODS

=head2 reset_password

Returns true is this is an admin login.

=head1 AUTHOR
       Eric Wolf        coyocanid@gmail.com

=head1 COPYRIGHT AND LICENSE

       Copyright (c) 2012 - 2019 Eric Wolf. All rights reserved.  This program is free software; you can redistribute it and/or modify it
       under the same terms as Perl itself.

=head1 VERSION
       Version 0.1  (Aug, 2019))

=cut
