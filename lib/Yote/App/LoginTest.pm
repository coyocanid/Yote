package Yote::App::LoginTest;

use strict;
use warnings;

use Yote::App;
use base 'Yote::App';

sub hello {
    return 1, "HELLO WORLD";
}

sub echo {
    my( $self, $args, $sess ) = @_;
    my $login = $sess->get_login;
    if( $login ) {
        return 1, $login->get_login;
    }
    return 0, 'must be logged in';
}

sub reign {
    my( $self, $args, $sess ) = @_;
    my $login = $sess->get_login;
    if( $login && $login->get_is_admin ) {
        return 1, "My Leige";
    }
    return 0, 'must be logged in';
    
}

1;
