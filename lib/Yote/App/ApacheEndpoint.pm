package Yote::App::ApacheEndpoint;

use strict;
use warnings;

use Data::Dumper;

use File::Temp qw/ tempfile /;

use APR::Table;
#use APR::Const -compile qw(:table);
use Apache2::Const qw( OK HTTP_BAD_REQUEST HTTP_NO_CONTENT HTTP_REQUEST_ENTITY_TOO_LARGE);
use Apache2::Response;
use Apache2::RequestRec;
use Apache2::RequestIO;

use Yote;
use Yote::App::Endpoint;

sub handler {
    my( $r ) = shift;

     my $use_req = 0;

    $r->set_keepalive;

    # apache lower limit for headers is 8k
    my $headers = $r->headers_in;

    # check for no input
    my $content_length = $headers->{'content-length'} || 0;
    if( $content_length == 0 ) {
        return Apache2::Const::HTTP_NO_CONTENT;
    }

    # find the boundary
    my( $boundary ) = ( $headers->{'content-type'} =~ /boundary=(.*)/ );
    unless( $boundary ) {
        return Apache2::Const::HTTP_BAD_REQUEST;
    }

    # fetch the rest, if needed
    my $buff;
    my $res = $r->read( $buff, $content_length );
    if( $res != $content_length ) {
        return Apache2::Const::HTTP_BAD_REQUEST;
    }

    # parse the content
    # TODO iterate over the parts with a read
    my $vars;
    if( $use_req ) {
        $vars = $r->post_vars;
        # do stuff with file
    } else {
        my( @buff ) = split( /[\n\r]{2}/, $buff );
        undef $buff;
        my( $insechead, $varname, $val, $tmpfh, $tmpfile, $filename );
        $vars = {};
        while( @buff ) {
            my $line = shift @buff;
            if( $line =~ /^--$boundary(--)?$/ ) {
                if( $1 && @buff ) {
                    #end reached, but more in buffer
                    return Apache2::Const::HTTP_BAD_REQUEST;
                }
                $insechead = 1;
                if( $varname ) {
                    $vars->{$varname} = $val;
                    if( $tmpfh ) {
                        push @{$vars->{file_handles}}, [$tmpfile,$tmpfh,$filename];
                        undef $tmpfh;
                    }
                }
            }
            elsif( $insechead ) {
                if( $line =~ /name="([^"]+)".*filename="([^"]+)"/ ) {
                    $varname  = $1;
                    $filename = $2;
                    ( $tmpfh, $tmpfile ) = tempfile();
                }
                elsif( $line =~ /form-data;.*name="([^"]+)"/ ) {
                    $varname = $1;
                    $val     = '';

                }
                elsif( $line eq '' ) {
                    $insechead = 0;
                }
            }
            elsif( $filename  ) {
                print $tmpfh $line;
            }
            else {
                $val .= $line;
            }
        }
    }
    my( $content_type, $content );
    if( $vars ) {
        ( $content_type, $content ) = Yote::App::Endpoint->handle( $vars, $ENV{HTTP_HOST} );
    } else {
        return Apache2::Const::HTTP_BAD_REQUEST;
    }

    $r->content_type( $content_type );
    $r->set_content_length( length($content) );
    $r->print( $content );
    $r->rflush();

    return Apache2::Const::OK;
} #handler

1;
