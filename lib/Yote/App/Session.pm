package Yote::App::Session;

#
# Session holds onto a session id and login.
# It is the filter for all outgoing and
# incoming objects. Any objects sent outbound
# throught its filter are noted and then considered
# legal for any inbound requests as targets or
# arguments.
#

use strict;
use warnings;
no warnings 'uninitialized';
use Time::HiRes qw(time);

use Yote::Obj;
use base 'Yote::Obj';

sub _init {
    my $self = shift;
    $self->set_cache({});
    $self->session_id;
}
sub clear {
    my $self = shift;
    my $cache = $self->get_cache;

    %$cache = ();
    $self->set_login(undef);
    $self->set__session_id(0);
} #clear

sub allow {
    my ($self, $obj ) = @_;
    my $tied = $self->store->tied_obj( $obj );
    if ($tied) {
	my $c = $self->get_cache; my $t = $self->store->tied_obj( $c );
	$self->get_cache->{$tied->[0]} = $obj;
    }
}
sub allows_id {
    my ($self, $id) = @_;
    return $self->get_cache->{$id};
}

sub stamp {
    my $self = shift;
    $self->set_updated( int(1000*time) );
}

sub unmarshal {
    my ($self, $inpt ) = @_;
    my $r = ref( $inpt );
    if( $r eq 'ARRAY' ) {
        return [map { $self->unmarshal($_) } @$inpt];
    }
    elsif( $r eq 'HASH' ) {
        return {map { $_ => $self->unmarshal($inpt->{$_}) } keys %$inpt};
    }
    my $t = substr( $inpt, 0, 1 );
    my $v = substr( $inpt, 1 );
    if( $t eq 'r' ) {
	return $self->get_cache->{$v};
    }
    elsif( $t eq 'v' ) {
        return $v;
    }
    return undef;
} #unmarshal

sub session_id {
    my $self = shift;
    my $sess_id = $self->get__session_id;
    if ( ! $sess_id ) {
        my $cache = $self->get_cache;
        %$cache = ();
        $self->set_login(undef);
        $sess_id = $self->set__session_id( "S_".int(rand(2**64)) );
    }
    return $sess_id;
} #session_id

sub empty_cache {
    my $self = shift;
    my $c = $self->get_cache;
    %$c = ();
} #empty_cache

1;

__END__


#
# Peforms the action on the requested targets with
# the given arguments.
#
sub do_action {
    my( $self, $target_ref, $action, $args ) = @_;
    my $target = $target_ref ? $self->get_cache->{$target_ref} : $self->get_app;
    if( $target && $self->get_app->may_use_method( $action, $target ) ) {
        my( $succ, $raw_ret ) = $target->$action( $self->_unmarshal( $args ), $self );
        if( $succ ) {
            my( $ret, $updates, $removals, $defs ) = $self->_datafy( $raw_ret );
            return $succ, $ret, $updates, $removals, $defs;
        }
        return $succ, $raw_ret;
    }
    return 0, 'bad input';
} #do_action

#
# Looks at the target requested and returns it from its
# cache
#
sub find_target {
    my( $self, $target ) = @_;
    return $self->get_app unless $target;
    return $self->get_cache->{$target};
} #find_target

sub _unmarshal {
    my( $self, $args ) = @_;
    my $r = ref( $args );
    if( $r eq 'ARRAY' ) {
        return [ map { $self->_unmarshal($_) } @$args ];
    }
    elsif( $r eq 'HASH' ) {
        return { map { $_ => $self->_unmarshal($args->{$_}) } keys %$args };
    }
    if( $args =~ /^([vr])(.*)/ ) {
        my( $des, $rest ) = ( $1, $2 );
        if( $des eq 'v' ) {
            return $rest;
        }
        return $self->get_cache->{$rest};
    }
    return undef;
} #_unmarshal

# examines the return value (being sent to the client)
# notes all the objects that touch the return value
# and caches those. It looks at the items in its cache
# and sees if the client needs an update of those.
# It returns a data structure
#  { ret => return value
#    data => { id => data item }
#    defs => { classname => list of allowed methods }
#  }
#
sub _datafy {
    my( $self, $obj ) = @_;

    my $seen = {};
    my $defs = {};

    # the cache stores objects that connect to objects sent across directly
    my $cache = $self->get_cache;
    my $store = $self->store;

    my $last_updated = $self->get__last_updated;

    my $updates = {};
    my( %no_update_needed, %gone );

    my $now = time;
    
    #
    # check if objects previously sent accross need updates.
    #
    for my $o (values %$cache) {
        # this fills the seen reference
        $self->_datafy_to_seen( $o, $store, $seen );
    }
    for my $id (keys %$seen) {
        my $o = $store->fetch($id);

        if( ! $cache->{$id} || $last_updated <= $store->last_updated( $o ) ) {
            my $cls = ref( $o );
            $updates->{$id} = $seen->{$id};
        }
        else {
            $no_update_needed{$id} = $o;
        }
    }

    #
    # See if there are any new top level objects going out.
    #
    my( @top_ids ) = $self->_extract_top_level_returns( $obj, $store );
    my( $has_new_top );
    for my $id (@top_ids) {
        unless( $cache->{$id} ) {
            my $o = $store->fetch( $id );
            $has_new_top = 1;
            $cache->{$id} = $o;
            $self->_datafy_to_seen( $o, $store, $seen );
        }
    }
    if( $has_new_top ) {
        for my $id (keys %$seen) {
            my $o = $seen->{$id};
            unless ( $no_update_needed{$id} ) {
                $updates->{$id} = $o;
            }
        }
    }

    # see if any non-top level objects are no longer reachable
    # by the client and delete those
    for my $id (keys %$cache) {
        unless( $updates->{$id} || $no_update_needed{$id} ) {
            $gone{$id} = 1;
            delete $cache->{$id};
        }
    }

    for my $id (keys %$updates) {
        my $o = $store->fetch($id);
        $cache->{$id} = $o;
        my $cls = ref( $o );
        if( $cls ne 'ARRAY' && $cls ne 'HASH' ) {
            $defs->{$cls} = $self->get_app->methods_for( $cls, $self );
        }
    }

    $self->set__last_updated( $now );

    my $ret = $self->_datafy_to_seen( $obj, $store, $seen );
    return $ret, $updates, [ keys %gone ], $defs;
} #_datafy

sub _extract_top_level_returns {
    my( $self, $obj, $store ) = @_;
    my $retref = ref( $obj );
    if( $retref ) {
        my $id = $store->existing_id( $obj );
        if( $id ) {
            return ($id);
        }
        if( $retref eq 'ARRAY' ) {
            return map { $self->_extract_top_level_returns( $_, $store ) } @$obj;
        }
        if( $retref eq 'HASH' ) {
            return map { $self->_extract_top_level_returns( $_, $store ) } values %$obj;
        }
        die "Got blessed non yote obj reference in return";
    }
    return ();
} #_extract_top_level_returns 


sub _datafy_to_seen {
    my( $self, $obj, $store, $seen ) = @_;

    my $r = ref( $obj );

    if( $r ) {
        # TODO - scramble ids with the session
        # uses existing ids? okey, not the best
        # but we can update this later at this point
        my $id = $store->existing_id( $obj );

        if( $id && $seen->{$id} ) {
            return "r$id";
        }

        my $thing = $store->_knot( $obj );

        if( $thing ) {
            if( $r eq 'ARRAY' ) {
                my $x = [];
                $seen->{$id} = $x;
                @$x = map { $self->_datafy_to_seen( $_, $store, $seen ) } @$obj;
                return "r$id";
            }
            elsif( $r eq 'HASH' ) {
                my $x = {};
                $seen->{$id} = $x;
                %$x = map { $_ => $self->_datafy_to_seen( $obj->{$_}, $store, $seen ) } keys %$obj;
                return "r$id";
            }
            my $fields = $obj->fields;
            my $vol_fields = $obj->vol_fields;
            my $x = {};
            $seen->{$id} = $x;
            %$x = map { $_ => $self->_datafy_to_seen( $obj->get($_), $store, $seen ) } grep { $_ !~ /^_/ } @$fields;
            my $cls = ref( $obj );
            $x->{_cls} = $cls;


            for my $vf (grep { /^[a-zA-Z]/ } @$vol_fields) {
                $x->{$vf} = $self->_datafy_to_seen( $obj->vol($vf), $store, $seen );
            }
            $obj->clearvols;
            return "r$id";
        }

        #not a yote object
        if( $r eq 'ARRAY' ) {
            return [ map { $self->_datafy_to_seen( $_, $store, $seen ) } @$obj ];
        }
        return { map { $_ => $self->_datafy_to_seen( $obj->{$_}, $store, $seen ) } keys %$obj };
    }
    return defined $obj ? "v$obj" : 'u';
} #_datafy_to_seen


=head1 NAME

Yote::App::Session - Session object for yote apps

=head1 SYNOPSIS


=head1 DESCRIPTION

=head1 METHODS

=head2 clear

Clears out the session id.

=head2 session_id

Returns the session id, calculating it if necessary.

=head2 logout

Cleans out the session and informs the app that the session is logged out.

=head2 find_target( target, app )

Returns the method target. If no target is given,
the app is returned. If a target is given that is not in the session
cache, undef is returned.

=head2 datafy( obj )

Translates a return argument into a hash with the fields
ret, data, remove and defs.

=head1 AUTHOR
       Eric Wolf        coyocanid@gmail.com

=head1 COPYRIGHT AND LICENSE

       Copyright (c) 2012 - 2019 Eric Wolf. All rights reserved.  This program is free software; you can redistribute it and/or modify it
       under the same terms as Perl itself.

=head1 VERSION
       Version 0.1  (Aug, 2019))

=cut
