package Yote::App::Util;

use strict;
use warnings;

#
# Takes { target, field, imgdata, file, destdir, destpath }
# and attaches the iamge to the target object on the field.
# if the field already has an image, that image is updated.
# file is [ tempfilename, filehanel, filename ]
#
warn "file parameter for save_image is a triplet array and should just be the temp file. see live spuc for examples of use";
sub save_image {
    my( $self, $args ) = @_;
    my $savefield = $args->{field};
    my $image = $args->{target}->get( $savefield );
    if( $image ) {
        my $origin_file = $image->get__origin_file;
        _write_image( $args->{imgdata}, $args->{file}, $origin_file );
    }
    else {
        $image = $self->make_picture( {
            destdir   => $args->{destdir},
            destpath  => $args->{destpath},
            imgdata  => $args->{imgdata},
            file     => $args->{file},
                                          } );
        $args->{target}->set( $savefield, $image );
    }
    return $image;
} #save_image

#
# Takes { destdir, destpath, original_name, imgdata, file }
# and attaches the iamge to the target object on the field.
# if the field already has an image, that image is updated.
#
sub _make_picture {
    my( $self, $args ) = @_;

    make_path( $args->{destdir} );
    my $pic = $self->store->create_container( 'App::Picture',
                                              {
                                                  extension      => 'png',
                                                  _original_name => $args->{original_name},
                                              } );
    my $origin_file = $pic->set__origin_file( "$args->{destdir}/$pic.png" );
    $pic->set_path( "$args->{destpath}/$pic.png" );
    $pic->set__origin_file( $origin_file );

    _write_image( $args->{imgdata}, $args->{file}, $origin_file );
    return $pic;
} #_make_picture


#
# writes the image data to the origin_file
#
sub _write_image {
    my( $imgdata, $file, $origin_file ) = @_;

    if( $imgdata ) {
        my $data = decode_base64($imgdata);
        open my $out, '>', $origin_file;
        print $out $data;
        close $out;
    }
    else {
        my $ft = File::Type->new();
        my $mag = new Image::Magick;
        my( $tmpFn, $fh, $fn ) = @$file;
        my $type = $ft->checktype_filename( $tmpFn );
        if( $type =~ /^image\/(gif|x-png|png|jpeg)/ ) {
            $mag->Read($tmpFn);
            $mag->Write( $origin_file );
        } else {
            die "unrecognizable file type '$type'";
        }
    }
} #_write_image


#
# Takes { start, length}, $list. Returns paginated part of list
#
sub _paginate_list {
    my( $self, $args, $list ) = @_;
    my $start = $args->{start};
    my $til   = $start + $args->{length};
    if( $start > $#$list ) { $start = $#$list }
    if( $til > $#$list ) { $til = $#$list }
    if( $start < 0 ) { $start = 0 }
    if( $til < 0 ) { $til = 0 }
    my $ret = { segment => [ grep { defined } map { $list->[$_] } ($start..$til) ], list_size => scalar(@$list) };
    return 1, $ret;
} #_paginate_list


1;
