package Yote::Endpoint;

use strict;
use warnings;

use Yote;
use Yote::App::Image;

use Data::Dumper;
use File::Copy;
use JSON;

our $YOTE;

sub load {
    my ($cls,$config) = @_;
    $YOTE //= Yote->load($config);
    return $YOTE && $cls;
}

sub unmarshal {
    my ($cls, $inpt ) = @_;
    my $r = ref( $inpt );
    if( $r eq 'ARRAY' ) {
        return [map { $cls->unmarshal($_) } @$inpt];
    }
    elsif( $r eq 'HASH' ) {
        return {map { $_ => $cls->unmarshal($inpt->{$_}) } keys %$inpt};
    }
    my $t = substr( $inpt, 0, 1 );
    my $v = substr( $inpt, 1 );

    # note, need a session to unmarshal references.
    if( $t eq 'v' ) {
        return $v;
    }
    return undef;
} #unmarshal

sub handle {
    my( $cls, $vars ) = @_;
    #    warn 'probably this part should not do any json encoding';
    $YOTE //= Yote->load;
    my $store = $YOTE->store;

    unless( $vars->{payload} ) {
        return 'text/json', '{"succ":0,"ret":"vfailed, no payload","data":{},"defs":{}}';
    }
    
    my $result;

    eval {
	print STDERR "\n\nPAY ($$) $vars->{payload}\n";
        my $inputs = decode_json( $vars->{payload} );

        my( $appname, $targetid, $action, $args, $sess_id )
            = @$inputs{qw(app target action args sess_id)};
        
        if( $appname && $action ) {
            my $app = $YOTE->fetch_app( $appname );
	    my $session = $app->find_session( $sess_id );
	    my $unmarshalled_args = ($session || $cls)->unmarshal($args);
	    my $target = $app;
	    if ($targetid && $session && $targetid ne "$app") {
		# the 'app' target is always allowed.
		$target = $session->allows_id( $targetid ) || $app;
	    }
	    if ($target && $target->is_allowed_method( $action ) ) {
		my $cache_time = $session->get_updated(0);

		my $web_root = $YOTE->get__config->{web_root};
		my $image_root = $YOTE->get__config->{image_root};
		my $upload_dir = "$web_root$image_root";
		
		my $uploader = sub {
		    if (my $desc = $vars->{uploads}->() ) {
			my ( $tempfn, $name, $handle ) = @$desc{qw(tempname name handle)};
			my ( $suff ) = ( $name =~ /\.([^.]+)$/ );
			my $img = $store->create_container( 'Yote::App::Image', {
			    name => $name
							    } );
			my $file_location = "$upload_dir/$img.png";
			my $image_location = "$image_root/$img.png";
			move( $tempfn, $file_location );
			$img->set_location( $image_location );
			return $img;
		    }
		    return undef;
		};
		
		my( $succ, $resp )
		    = $target->$action( $unmarshalled_args, $session, $uploader );
		$session->stamp;

		my $new_sess_id = $session->session_id;
		
		$app->register_session( $session );
		
		my $data = {};
		my $defs = {};
		my $marshal;
		$marshal = sub {
		    my $item = shift;

		    if ( !defined $item ) { return 'u'; }
		    my $r = ref( $item );
		    if ( !$r ) { return "v$item"; }

		    my $id = $store->existing_id( $item );
		    if ($id) {
			if ($data->{$id}) { return "r$id"; }
			if ($r =~ /^(ARRAY|HASH)$/) {
			    # nothing to do for array or hash
			}
			elsif ($item->isa( 'Yote::Obj' )) {
			    $defs->{$r} //= $item->allowed_methods;
			}
			else {
			    warn "Object $item is '$r' not Yote::Obj\n";
			}
			$session && $session->allow( $item );
		    } else {
			$id = "$item";
			if ($data->{$id}) { return "r$id"; }
		    }
		    my $marshalled_data = $r eq 'ARRAY' ? [] : {};
		    $data->{$id} = [ $r, $marshalled_data ];
		    if ($r eq 'ARRAY') {
			push @$marshalled_data, map { $marshal->($_) } @$item;
		    }
		    elsif ($r eq 'HASH') {
			for my $k (keys %$item) {
			    # exclude fields prepended with underscore
			    if (index( $k, '_' ) != 0) {
				$marshalled_data->{$k} = $marshal->( $item->{$k} );
			    }
			}
		    } else {
			my $source = $item->[1];
			for my $k (keys %$source) {
			    # exclude fields prepended with underscore
			    if (index( $k, '_' ) != 0) {
				$marshalled_data->{$k} = $marshal->( $item->get($k) );
			    }
			}
		    }
		    return "r$id";
		}; #marshal
		
		# include updated items in the return data
		my $sess_cache = $session->get_cache;
		my $scthing = $session->store->tied_obj( $sess_cache );
		for my $val (values %$sess_cache) {
		    my $obj = $session->store->tied_obj( $val );
#		    print STDERR "CACHE CHECK ".ref($obj)." ".$session->store->existing_id($obj)." : $obj->[Data::ObjectStore::Container::META]{updated} >= $cache_time\n";
		    if ($obj->[Data::ObjectStore::Container::META]{updated} >= $cache_time) {
#			print STDERR "   *** UPDATING ***\n";
			$marshal->( $val );
		    } else {
		    }
		}

		my $ret = $marshal->( $resp );

		$YOTE->store->save;
		$YOTE->store->unlock;

#		print STDERR Data::Dumper->Dump([$session->get_cache,"SESSY"]);

		# forget the session stuff for now
		$result = encode_json( {
		    succ  => $succ,       # 1 success, 0 failure
		    ret   => $ret,        # value data ( u - undef, vValue - scalar Value, rRef - ref label
		    data  => $data,       # { ref : { key value data }
		    defs  => $defs,       # { class : [methodlist] }
		    sess_id => defined( $new_sess_id ) ? $new_sess_id : $sess_id,  # may be undef, 0 or a positive integer
				  } );
	    } #allowed method
	    elsif ($target) {
		$result = qq~{"succ":0,"ret":"vfailed : '$action' not allowed by '$appname'","data":{},"defs":{}}~;
	    }
	    else {
		$result = qq~{"succ":0,"ret":"vfailed : target '$target' not found","data":{},"defs":{}}~;
	    }
	}
    };
    if( $@ ) {
        warn "got error : $@ $!\n";
	$result = qq~{"succ":0,"ret":"vfailed : $@ $!","data":{},"defs":{}}~;
    }
    print STDERR "\nRES ($$) : $result\n\n\n";
    return "text/json", $result;

} #handle

1;
