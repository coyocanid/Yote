package Yote::Test;

use strict;
use warnings;

use Data::Dumper;
use Yote;
use Yote::Endpoint;

use base 'Yote::App';

sub _load {
    my $self = shift;
    $self->SUPER::_load;
    $self->fix_methods( qw ( 
			check_login
			check_email
			login
			logout
			has_login
			install_message
			install_special_message
			install_title
			last_upload
			load
			produce
			reset
			signup
			show_arry
			stamp
			store_obj
			structs
			upload
			) );
}


sub reset {
    my ($self,$args,$sess) = @_;

    # clear all sessions attached to this test
    for my $sess (values %{$self->get__session}) {
	$sess->clear;
    }
    
    #
    # code like this should only be for tests
    # because you dont want apps to mess with
    # things above them like yote which encompasses them.
    #
    
    my $root = $self->store->fetch_root;
    my $config = $root->get_yote->get__config;
    $root->set_yote( undef );
    $self->store->save;

    undef $Yote::SINGLETON;
    undef $Yote::Endpoint::YOTE;
    my $yote = Yote->activate( $config );
    Yote::Endpoint->load( $config );
    my $app = $yote->get_active_apps->{test};

    # reset the session, but add it to the newly created app
    $sess->clear;
    $app->get__session({})->{$sess->session_id} = $sess;
    
    return 1, $app;
}

sub last_upload {
    my $self = shift;
    return 1, $self->get_last_uploaded;
} #last_upload

sub upload {
    my( $self, $args, $session, $uploader ) = @_;
    if ($session && $session->get_login) {
	my ($img) = ($uploader && $uploader->());
	if( $img ) {
	    $self->set_last_uploaded( $img );
	}
	return 1, $img;
    }
    return 0, 'must be logged in';
} #upload

sub install_message {
    my( $self, $message ) = @_;
    return 1, $self->set_message( $message );
} #install_message


sub install_special_message {
    my( $self, $message, $session ) = @_;
    if ($session && $session->get_login) {
	return 1, $self->set_special_message( $message );
    }
    return 0, 'need an account to set the message';
} #install_special_message

sub install_title {
    my( $self, $message ) = @_;
    return 1, $self->set_title( $message );
}

# not in allowed
sub cant_call_me { 
    return 1, 'you actually called me';
}

sub produce {
    my ($self,undef,$session) = @_;
    if ($session && $session->get_login) {
	return 1, $self->store->create_container( 'Yote::Obj' );
    }
    return 0, 'must be logged in to produce';
}

sub structs {
    my ($self) = @_;
    my $arry = [];
    my $hash = { arry => $arry };
    $hash->{hash} = $hash;
    push @$arry, $arry, $hash;
    return 1, $arry;
} #structs

sub stamp {
    my( $self, $obj, $session ) = @_;
    my $login = $session && $session->get_login;
        
    if( $login && $obj ) {
	my $old_count = $obj->get_count(0);

	if ($old_count == 1) {
	    $obj->set_info( { got_a_stamp => 1 + $old_count } );
	    $obj->set_info_arry( [1 + $old_count] );
	}
	elsif( $old_count > 1 ) {
	    $obj->get_info->{got_a_stamp} = 1 + $old_count;
	    $obj->get_info_arry->[0] = 1 + $old_count;
	}
	$obj->set_count( 1 + $old_count );
	$login->set_stamp_count( 1 + $login->get_stamp_count(0) );
	return 1, $obj;
    }
    return 0, 'must be logged in with a valid object';
}

sub show_arry {
    my( $self, $arry ) = @_;
    return 0, 'needs an array' unless ref($arry) eq 'ARRAY';
    return 1, [$self->set_arry_count( $self->get_arry_count(0) + @$arry )];
}

sub store_obj {
    my( $self, $obj ) = @_;
    $self->add_to_stored( $obj );
    return 1, $self;
}

sub update {
    my( $self, $keyval ) = @_;
    
}

1;

__END__

so, an app has the following parts :

  the app class

  the html

  the javascript
