package Yote::Client::Obj;

use strict;
use warnings;

sub new {
    my ($pkg, $args) = @_;
    return bless {
	app     => $args->{app},
	class   => $args->{class},
	client  => $args->{client},
	data    => {},
	id      => $args->{id},
	methods => { map { $_ => 1 } @{$args->{methods}||[]} },
    }, $pkg;
} #new

sub AUTOLOAD {
    my ( $self, $arg ) = @_;
    my $auto = our $AUTOLOAD;
    my ($func) = ( $auto =~ /([^:]+)$/ );
    if ($self->{methods}{$func}) {
	return $self->{client}->call( $self->{app},
				      $self->{id},
				      $func,
				      $arg );
    }
    if ($func =~ /^get_(.*)/ ) {
	no warnings 'uninitialized';
	my ($pref, $val ) = ( $self->{data}{$1} =~ /^(.)(.*)/ );
	return $val if $pref eq 'v';
	return undef if $pref eq 'u';
	return $self->{client}{app2cache}{$self->{app}}{$val};
	use warnings 'uninitialized';
    }
    
    if ($func ne 'DESTROY') {
	warn "called unallowed function $func\n";
    }
    return 0, "failed : '$func' not allowed by '$self->{app}'";
} #AUTOLOAD

1;
