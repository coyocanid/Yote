package Yote::Admin;

use strict;
use warnings;

use Yote::App::Basic;
use base 'Yote::App::Base';

sub _init {
    my $self = shift;
    $self->set_path_to_app( {} );
    $self->set_logins( {
        names  => {},
        emails => {},
                            } );
}



1;
