package Blogger::Account;

use strict;
use warnings;

use Yote::Obj;
use base 'Yote::App::Account';

sub _load {
    my $self = shift;
    $self->get_avatars([]);
    $self->fix_methods( qw ( 
			remove_avatar
			use_avatar
			update_bio
			upload_avatars
			) );
}
