import { act, Paginator, match, matches, byId, IO } from "/js/tools.js";
import "/js/vue_components.js";

class Admin {
    constructor() {
        this.io = new IO( {
            app : 'Admin',
        } );
    }
} //Admin class

let admin = new Admin();

Vue.component('main_admin', {
    data : function() {
        return { admin };
    },
    template : `<div>
                 <h2>Galaxy Admin</h2>
                 <div v-if="admin.io.session">
                   Domain Path App
                 </div>
                 <div v-else>
                   <login io="admin.io"><h3>Admin Log In</h3></login> 
                 </div>
                </div>`,
} ); //main_admin component
admin.io.session_login()
    .then( sess => {
        new Vue( {
            el : '#main',
            template : `<main_admin></main_admin>`,
        } );
    } )
    .catch( err => {
        new Vue( {
            el : '#main',
            template : `<main_admin></main_admin>`,
        } );
    } );


