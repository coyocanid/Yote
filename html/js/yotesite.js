import { act, Paginator, match, matches, byId } from "/yote/js/tools.js";
import { fetchYoteApp } from '/yote/js/yote.js';


fetchYoteApp( '/cgi-bin/app-endpoint', 'pageCounter' )
    .then( counter => {
        console.log( 'counter', counter );
        counter.update_counter();
        new Vue({
            el : '#footer',
            data : function() { return { counter } },
            template : `<footer>This has been viewed 
                           {{ counter.data.hits }} times.
                           <button type="button"
                                   v-on:click="counter.update_counter()">Add a hit</button>
                        </footer>`,
        } );
    } );

fetchYoteApp( '/cgi-bin/app-endpoint', 'pageCounter2' )
    .then( counter => {
        console.log( 'counter', counter );
        counter.update_counter();
        new Vue({
            el : '#login',
            data : function() { return { counter } },
            template : `<span>hitz {{ counter.data.hits }}</span>`,
        } );
    } );


fetchYoteApp( '/cgi-bin/app-endpoint', 'pageCounter3' )
    .then( counter => {
        console.log( 'counter', counter );
        counter.update_counter();
        new Vue({
            el : '#pc2',
            data : function() { return { counter } },
            template : `<span>hitz {{ counter.data.hits }}</span>`,
        } );
    } );

fetchYoteApp( '/cgi-bin/app-endpoint', 'pageCounter4' )
    .then( counter => {
        console.log( 'counter', counter );
        counter.update_counter();
        new Vue({
            el : '#pc3',
            data : function() { return { counter } },
            template : `<span>hitz {{ counter.data.hits }}</span>`,
        } );
    } );

