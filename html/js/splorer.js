import { act, Paginator, match, matches, byId } from "/spuc/js/tools.js";

let z = 0; // z-order
let lastFocus; // 

window.document.body.onclick = function(ev) {
    
};
window.document.body.onkeyup = function(ev) {
    if( ev.key === 'Escape' ) {
        if( lastFocus ) {
            lastFocus.show();
            lastFocus = undefined;
        }
    }
};

Vue.component('renamer', {
    props: [ 'field' ],
    template : `<span class="renamer">
                   <input type="text" class="hide"
                          v-on:keyup.enter="update"
                          v-on:keyup.escape="show"
                          v-bind:value="field">
                   <span class="hide click" v-on:click="show">&timesb;</span>
                   <span class="click" style="background:#FFE;border:1px solid black"
                         v-on:click="edit">
                      {{ field }}
                   </span>
                </span>`,
    updated : function() {
        console.log( "Updated " + this.field, this );
    },
    methods : {
        show : function() {
            this.$el.children[0].classList.add( 'hide' );
            this.$el.children[1].classList.add( 'hide' );
            this.$el.children[2].classList.remove( 'hide' );
        },
        edit : function() {
            this.$el.children[2].classList.add( 'hide' );
            this.$el.children[1].classList.remove( 'hide' );
            this.$el.children[0].value = this.field;
            this.$el.children[0].classList.remove( 'hide' );
            this.$el.children[0].focus();
            this.$emit( 'click' );
            lastFocus = this;
        },
        update : function( ev ) {
            this.$emit( 'update', ev.target.value );
            this.show();
        }        
    },
} ); //renamer

Vue.component('valuer', {
    props: [ 'node', 'field' ],
    data : function() {
        let value = this.$props.node.data[this.$props.field] || 'u';
        let manager = this.$attrs.manager;
        return { value, manager };
    },
    template : `<span class="valuer">
                  <span v-if="value.substring(0,1)=='v'">
<!--                    <input type="text"
                           v-on:change="update"
                           v-bind:value="value.substring(1) : '?'"> -->
                        "{{ value.substring(1) }}"
                  </span>
                  <a href="#" v-else v-on:click="manager.open(value.substring(1),field,node)">{{ value.substring(1) }}</a>
                  <button type="button">change</button>
                </span>`,
    methods : {
        update : function( ev ) {
            this.$emit( 'update', ev.target.value );
        },       
    },
} ); //valuer

Vue.component('node', {
    data : function() {
        let node = this.$attrs.node;
        let manager = this.$attrs.manager;
        return {node,manager,error:undefined,position:undefined,showchunk:10,showing:10};
    },
    methods : {
        rename_field : function( field, newfield ) {
            this.manager.rename_field( field, newfield, this.node, this );
        },
        tag : function( ev ) {
            this.manager.tag( ev, this.node, this );
        },
        collapse : function( ev ) {
            this.$el.classList.toggle('collapsed');
        },
        dragstart : function( ev ) {
            this.position = { x : ev.clientX,
                              y : ev.clientY,
                              left : this.$el.offsetLeft,
                              top : this.$el.offsetTop };
        },
        dragend : function( ev ) {
            this.$el.style.position = 'absolute';
            let newl = this.position.left + (ev.clientX - this.position.x);
            if( newl < this.$el.parentElement.offsetLeft ) newl = this.$el.parentElement.offsetLeft;
            let newt = this.position.top + (ev.clientY - this.position.y);
            if( newt < this.$el.parentElement.offsetTop ) newt = this.$el.parentElement.offsetTop;
            console.log( newl + ' / ' + newt, this.$el.parentElement.offsetTop);
            this.$el.style.left = newl + 'px';
            this.$el.style.top = newt + 'px';
            if( this.$el.style['z-index'] < z || ! this.$el.style['z-index'] )
                this.$el.style['z-index'] = ++z;
        },
        preview_fields : function() {
            let pv = [];
            for( let i=0; i<this.showing && i<this.node.fields.length; i++ ) {
                pv.push( this.node.fields[i] );
            }
            return pv;
        },
        has_more : function() {
            return this.node.fields.length > this.showing;
        },
        has_way_more : function() {
            return this.node.fields.length > 2*this.showing;
        }
    },
    updated : function() {
        if( ! this.$el.style['z-index'] ) {
            this.$el.style['z-index'] = this.node.z;
        }
    },
    template : `<div class="node" 
                     style="border:solid 1px; display: inline-block; background-color:wheat;"
                     v-on:dragstart="dragstart"
                     v-on:dragend="dragend"
                     draggable="true">
                   <div class="row shove" style="cursor:move;">
                       <span style="font-size:smaller">{{ node.id }}</span>
                       <renamer  v-bind:field="node.tag"
                                 v-on:update="tag"
                                 v-on:click="error=undefined"
                                 v-if="node.tag">{{ node.tag }}</renamer>
                       <div syle="display: inline-block;">
                          <span class="click collapse" 
                                v-on:click="error=undefined;collapse()"
                                >&minusb;</span>
                          <span class="click uncollapse" 
                                v-on:click="error=undefined;collapse()"
                                >&plusb;</span>
                          <span class="click" 
                                v-on:click="error=undefined;manager.close(node.id)"
                                v-if="node.id > 2">&timesb;</span>
                       </div>
                   </div>
                   <span v-if="error!==undefined" class="error">ERROR : {{ error }}
                     <span class="click" v-on:click="error = undefined">&timesb;</span>
                   </span>
                   <span v-if="Array.isArray(this.node.data)">Array with {{ this.node.fields.length }} items</span>
                   <span v-else>{{ this.node.class }} with {{ this.node.fields.length }} fields</span>
                   <div v-if="Array.isArray(this.node.data)" class="collapse">
                     <ol>
                       <li v-for="field in this.preview_fields()" v-bind:key="field">
                         <valuer v-bind:field="field" 
                                 v-bind:node="node"
                                 v-bind:manager="manager"
                                 v-on:click="error=undefined;"
                                 v-on:change="update_field(node.id,field,$event)"
                              ></valuer>
                       </li>
                     </ol>
                     <div v-if="this.has_more()"><a href="#" v-on:click="showing*=2">...Show {{ showing+showchunk > node.fields.length ? node.fields.length - showing : showing+showchunk }} more...</a></div>
                     <div v-if="this.has_way_more()"><a href="#" v-on:click="showing=node.fields.length">...Show all...</a></div>
                   </div>
                   <div v-else class="collapse">object links
                     <ul>
                       <li v-for="field in this.preview_fields()" v-bind:key="field">
                         <renamer v-bind:field="field"
                                  v-on:click="error=undefined;"
                                  v-on:update="rename_field(field,$event)"
                              ></renamer> - 
                         <valuer v-bind:field="field" 
                                 v-bind:node="node"
                                 v-bind:manager="manager"
                                 v-on:click="error=undefined;"
                                 v-on:change="update_field(node.id,field,$event)"
                              ></valuer>
                       </li>
                     </ul>
                     <div v-if="this.has_more()"><a href="#" v-on:click="showing*=2">...Show {{ showing+showchunk > node.fields.length ? node.fields.length - showing : showing+showchunk }} more...</a></div>
                     <div v-if="this.has_way_more()"><a href="#" v-on:click="showing=node.fields.length">...Show all...</a></div>
                   </div>
                </div>
               `,    
}); //node

class Manager {
    constructor() {
        this.nodes = [];
        this.id2node = {};
        this.id2tag = {};
        this.tag2id = {};
        this.error = undefined;
        this.msg = undefined;
        this.nodes.forEach( n => {
            this.id2node[n.id] = n;
            this.tag2id[n.tag] = n.id;
            this.id2tag[n.id] = n.tag;
            console.log( n, "Adding" );
        });
        console.log(this,'CONY');
    } //constructor
    open( query, field, from_node ) {
        if( this.id2node[ query ] ) {
            
        } else {
            let that = this;
            act( { app :'App::Yote::DB',
                   action : 'load',
                   args : query } ).then( resp => {
                       this.error = undefined;
                       if( resp.id ) {
                           resp.fields = Object.keys( resp.data ).sort();
                           resp.z = ++z;
                           resp.tag = that.id2tag[query] || from_node ? from_node.tag + '/' + field : resp.path ? resp.path.substr(1) : '#'+query;
                           that.tag2id[resp.tag] = query;
                           that.id2tag[query] = resp.tag;
                           that.id2node[query] = resp;
                           that.nodes.unshift( resp );
                       } else {
                           this.msg = query + ' = ' + resp.data;
                       }
            } ).catch( err => {
                this.error = err;
            } );
        }
    } //open
    close( id ) {
        this.nodes.splice( this.nodes.findIndex( v => v.id === id ), 1 );
        delete this.id2node[ id ];
    } //close
    rename_field(field,newfield,node,nodebox) {
        newfield = newfield.trim();
        if( field !== newfield ) {
            if( undefined !== node.data[newfield] ) {
                nodebox.error = "field '"+newfield+"' already taken";
            } else if(!newfield.match( /^[_a-zA-Z][_a-zA-Z0-9]*$/ ) ) {
                nodebox.error = "invalid field '"+newfield+"'. Field must contain only alphanumerics and not start with a number";
            } else {
                node.data[newfield] = node.data[field];
                node.fields.splice( node.fields.findIndex( v => v === field ), 1, newfield );
                delete node.data[field];
                nodebox.error = undefined;
            }
        }
    } //rename_field
    tag(tag,node,nodebox){
        console.log( tag, node, nodebox, this, "RENTAG" );
        if( this.tag2id[tag] && this.id2node[this.tag2id[tag]] ) {
            if( nodebox )
                nodebox.error = "tag '" + tag + "' already taken";
        } else {
            node.tag = tag;
            this.tag2id[tag] = node.id;
            this.id2tag[node.id] = tag;
            nodebox.error = undefined;
        }
    } //tag
} //Manager

let manager = new Manager();
Vue.component('nodes', {
    data : function() {
        return { manager, lookup : 0, target : undefined };
    },
    methods : {
        beep: function(tar) {
            this.target = tar;
            console.log( this.target );
        },
        x: function(x) { console.log( x,"X" ) },
    },
    template : `<div>
                  <div class="row">
                     <input type="text" v-on:keyup="target = $event.target.value"
                                        v-on:change="target = $event.target.value;manager.open(target)">
                     <button v-on:click="manager.open(target)"
                              >Find</button>
                     <span v-if="manager.error!==undefined" class="error">ERROR : {{ manager.error }}</span>
                     <span v-if="manager.error!==undefined" class="click" v-on:click="manager.error = undefined">&timesb;</span>
                     <span v-if="manager.msg!==undefined" class="msg">
                         {{ manager.msg }}
                         <span v-if="manager.msg!==undefined" class="click" v-on:click="manager.msg = undefined">&timesb;</span>
                     </span>
                  </div>
                  <div class="nodes page">
                   <node v-for="node in this.manager.nodes"
                         v-bind:node="node" 
                         v-bind:manager="manager" 
                         v-bind:key="node.id">
                   </node>
                  </div>
                </div>`,
} ); // nodes

let nodes = [];

new Vue({
    el : '#main',
    data : function() { 
        return { nodes } },
    template : `<nodes v-bind:nodes="nodes"></nodes>`,
});

manager.open( '/' );
