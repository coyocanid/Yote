#!/usr/bin/perl
use strict;
use warnings;

use Carp;
use File::Temp qw/ :mktemp tempdir /;
use JSON;

use lib '/home/wolf/open_source/recordstore/lib';
use lib '/home/wolf/open_source/objectstore/lib';
use lib './lib';
use lib 't/lib';

use Yote;

use Test::More;

$SIG{ __DIE__ } = sub { Carp::confess( @_ ) };


use Yote::App::Endpoint;

my( $dir, $yote );
sub init_test {
    my( $yaml, $keepdir ) = @_;
    unless( $keepdir ) {
        $dir = tempdir( CLEANUP => 1 );
    }
    $yaml =~ s!TMPDIR!$dir/data_store!gs;
    $ENV{YOTE_ROOT_DIRECTORY} = $dir;
    open my $out, '>', "$dir/yote-conf.yaml";
    print $out "---\n$yaml\n";
    close $out;
    $yote = Yote::App::Endpoint->init( $dir );
    return $yote;
}

sub expect {
    my( %args ) = @_;
    my( $test, $inputs, $outputtype, $outputs, $sess_id, $updater ) = @args{ 'test', 'inputs', 'outputtype', 'outputs', 'sess_id', 'updater' };
#    $inputs->{sess_id} = "v$inputs->{sess_id}" if $inputs->{sess_id};
    my( $outt, $data ) = Yote::App::Endpoint->handle( { payload => to_json( $inputs ) }, 'localhost' );
    my $result = from_json($data);
    $result->{sess_id} = substr( $result->{sess_id}, 1 );
#    print STDERR " SEND <---- ".to_json($inputs)."\nGOT --> $data\n\n";
    my $new_sess_id = $result->{sess_id};
    if( $updater ) {
        $updater->( $outputs, $result );
    }
#    print STDERR Data::Dumper->Dump([$outputs]);
    if( $outputs->{defs} ) {
        for my $cls (keys %{$outputs->{defs}}) {
            $outputs->{defs}{$cls} = [ sort @{$outputs->{defs}{$cls}} ];
        }
    }
    if( $outputs->{remove} ) {
        $outputs->{remove} = [ sort @{$outputs->{remove}} ];
    }
    if( $result->{defs} ) {
        for my $cls (keys %{$result->{defs}}) {
            $result->{defs}{$cls} = [ sort @{$result->{defs}{$cls}} ];
        }
    }
    if( ! $outputs->{sess_id} ) {
        delete $result->{sess_id};
    }
    if( $outputtype ) {
#        is( $outt, $outputtype, "output type of $test" );
    }
    is_deeply( $result, $outputs, "output of $test" );
    return $new_sess_id;
} #expect

$yote = init_test( <<"END" );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  'localhost':
    APPS:
      'pagecounter': 
        MAIN-APP-CLASS: 'PageCounter'
        APP-METHODS:
          'PageCounter':
            PUBLIC:
               - update_counter
               - fetch
END


my( $outt, $data ) = Yote::App::Endpoint->handle( {}, 'localhost' );
is( $outt, 'text/json', "no payload still returns json" );
is_deeply( from_json($data), { succ => 0, ret => "failed" }, "no payload returns failed json" );

( $outt, $data ) = Yote::App::Endpoint->handle( { payload => to_json( { foo => "BAR" } ) }, 'localhosty' );
is( $outt, 'text/json', "no payload still returns json" );
is_deeply( from_json($data), { succ => 0, ret => "failed" }, "no payload returns failed json" );

my $app = $yote->domain('localhost')->get_apps->{pagecounter};
my $app_id = "$app";

my $sess_id = expect( test => "PageCounter fetch",
                      inputs => { app => "pagecounter", action => "fetch" },
                      outputtype => 'text/json',
                      outputs => {
                          defs => { 'PageCounter' => ["update_counter","fetch"] },
                          ret => "r$app_id",
                          data => { $app_id => { _cls => "PageCounter" } },
                          remove => [],
                          succ => 1,
                      },
    );
ok( $sess_id > 0, "got a session id" );
my $old_sess_id = $sess_id;

$sess_id = expect( test => "PageCounter refetch one",
                   inputs => { app => "pagecounter",
                               sess_id => $sess_id,
                               action => "fetch" },
                   outputtype => 'text/json',
                   outputs => {
                       # fetch always empties out the session cac he
                       defs => { 'PageCounter' => ["update_counter","fetch"] },
                       ret => "r$app_id",
                       data => { $app_id => { _cls => "PageCounter" } },
                       remove => [],
                       succ => 1,
                   },
    );

is( $sess_id, $old_sess_id, 'same session' );

$sess_id = expect( test => "PageCounter refetch two anew",
                   inputs => { app => "pagecounter",
                               action => "fetch" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => { 'PageCounter' => ["update_counter","fetch"] },
                       ret => "r$app_id",
                       data => { $app_id => { _cls => "PageCounter" } },
                       remove => [],
                       succ => 1,
                   },
    );

isnt( $sess_id, $old_sess_id, 'same session' );
$old_sess_id = $sess_id;

$sess_id = expect( test => "PageCounter rehit",
                   inputs => { app => "pagecounter",
                               sess_id => $sess_id,
                               action => "update_counter" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => { 'PageCounter' => ["update_counter","fetch"] },
                       ret => "v1",
                       data => { $app_id => { _cls => "PageCounter",
                                              hits => "v1" } },
                       remove => [],
                       sess_id => $sess_id,
                       succ => 1,
                   },
    );
is( $sess_id, $old_sess_id, 'still same session' );

$sess_id = expect( test => "PageCounter rehit",
                   inputs => { app => "pagecounter",
                               sess_id => $sess_id,
                               action => "update_counter" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => { 'PageCounter' => ["update_counter","fetch"] },
                       ret => "v2",
                       data => { $app_id => { _cls => "PageCounter",
                                              hits => "v2" } },
                       remove => [],
                       sess_id => $sess_id,
                       succ => 1,
                   },
    );
is( $sess_id, $old_sess_id, 'still same session' );

$yote = init_test( <<"END" );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  'localhost':
    APPS:
      'pagecounter': 
        MAIN-APP-CLASS: 'PageCounter'
        APP-METHODS:
          'PageCounter':
            PUBLIC:
               - update_counter
               - update
               - fetch
END

$app = $yote->domain('localhost')->get_apps->{pagecounter};
$app_id = "$app";

my $sess_one_id = expect( test => "PageCounter one fetch",
                   inputs => { app => "pagecounter",
                               action => "fetch" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => { 'PageCounter' => ["update_counter","update","fetch"] },
                       ret => "r$app_id",
                       data => { $app_id => { _cls => "PageCounter", } },
                       remove => [],
                       succ => 1,
                   },
    );
my $old_sess_one_id = $sess_one_id;
print STDERR Data::Dumper->Dump([$sess_one_id,"ARF"]);
ok( $sess_one_id > 0, "got a session id" );

$app = $yote->domain('localhost')->get_apps->{pagecounter};
$app_id = "$app";

$sess_one_id = expect( test => "PageCounter one refetch",
                   inputs => { app => "pagecounter", action => "fetch", sess_id => $sess_one_id },
                   outputtype => 'text/json',
                   outputs => {
                       defs => { 'PageCounter' => ["update_counter","update","fetch"] },
                       ret => "r$app_id",
                       data => { $app_id => { _cls => "PageCounter", } },
                       remove => [],
                       succ => 1,
                   },
    );
is( $sess_one_id, $old_sess_one_id, 'same session id' );


my $sess_two_id = expect( test => "PageCounter two fetch",
                          inputs => { app => "pagecounter", action => "fetch" },
                          outputtype => 'text/json',
                          outputs => {
                              defs => { 'PageCounter' => ["update_counter","fetch","update"] },
                              ret => "r$app_id",
                              data => { $app_id => { _cls => "PageCounter" } },
                              remove => [],
                              succ => 1,
                          },
    );
my $old_sess_two_id = $sess_two_id;
ok( $sess_two_id > 0, "got a session id" );

$sess_one_id = expect( test => "PageCounter one hit",
                   inputs => { app => "pagecounter",
                               target => $app_id,
                               sess_id => $sess_one_id,
                               action => "update_counter" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => { 'PageCounter' => ["update_counter","fetch","update"] },
                       ret => "v1",
                       data => { $app_id => { _cls => "PageCounter",
                                              hits => "v1" } },
                       sess_id => $sess_one_id,
                       remove => [],
                       succ => 1,
                   },
    );
is( $old_sess_one_id, $sess_one_id, "session id one did not change" );

$sess_one_id = expect( test => "PageCounter update after one hit",
                   inputs => { app => "pagecounter",
                               target => $app_id,
                               sess_id => $sess_one_id,
                               action => "update" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => {},
                       ret => "u",
                       data => {},
                       sess_id => $sess_one_id,
                       remove => [],
                       succ => 1,
                   },
    );
is( $old_sess_one_id, $sess_one_id, "session id one did not change" );


$sess_two_id = expect( test => "PageCounter two hit",
                   inputs => { app => "pagecounter",
                               target => $app_id,
                               sess_id => $sess_two_id,
                               action => "update_counter" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => { 'PageCounter' => ["update_counter","fetch","update"] },
                       ret => "v2",
                       data => { $app_id => { _cls => "PageCounter",
                                              hits => "v2" } },
                       sess_id => $sess_two_id,
                       remove => [],
                       succ => 1,
                   },
    );
is( $old_sess_two_id, $sess_two_id, "session id two did not change" );

$sess_one_id = expect( test => "PageCounter one hit",
                   inputs => { app => "pagecounter",
                               target => $app_id,
                               sess_id => $sess_one_id,
                               action => "update" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => { 'PageCounter' => ["update_counter","fetch","update"] },
                       ret => "u",
                       data => { $app_id => { _cls => "PageCounter",
                                              hits => "v2" } },
                       sess_id => $sess_one_id,
                       remove => [],
                       succ => 1,
                   },
    );
is( $old_sess_one_id, $sess_one_id, "session id one did not change after update" );

$yote = init_test( <<"END" );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  'localhost':
    APPS:
      'logintest': 
        MAIN-APP-CLASS: 'LoginTest'
        APP-METHODS:
          'LoginTest':
            PUBLIC:
              - hello
              - fetch
              - update
              - signup
              - login
            LOGIN-REQUIRED:
              - echo
              - toggle
            ADMIN-REQURED:
              - reign
          'Widget':
            LOGIN-REQUIRED:
              - hi
END

$app = $yote->domain('localhost')->get_apps->{logintest};
$app_id = "$app";

$sess_id = expect( test => "LoginTest B fetch",
                   inputs => { app => "logintest", action => "fetch" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => { 'LoginTest' => [qw( hello fetch update signup login )] },
                       ret => "r$app_id",
                       data => { $app_id => { _cls => "LoginTest", } },
                       remove => [],
                       succ => 1,
                   },
    );
ok( $sess_id > 0, "got a session id" );


$old_sess_id = $sess_id;
$sess_id = expect( test => "LoginTest B fetch again",
                   inputs => { app => "logintest",
                               sess_id => $sess_id,
                               action => "hello" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => {},
                       ret => "vHELLO WORLD",
                       data => {},
                       remove => [],
                       succ => 1,
                   },
    );
is( $sess_id, $old_sess_id, "same sess id after refetch" );

$old_sess_id = $sess_id;
$sess_id = expect( test => "LoginTest B forbidden echo",
                   inputs => { app => "logintest",
                               sess_id => $sess_id,
                               action => "echo" },
                   outputtype => 'text/json',
                   outputs => {
                       ret => "failed",
                       succ => 0,
                   },
    );
is( $sess_id, $old_sess_id, "session id after forbidden echo error" );


$old_sess_id = $sess_id;
my $login_id;
$sess_id = expect( test => "LoginTest signup",
                   inputs => { app => "logintest",
                               sess_id => $sess_id,
                               args => { login => "vfoo",
                                         password => "vbarbarbarbar",
                                         email => 'vfoo@localhost' },
                               action => "signup" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => { 'Yote::App::Login' => [] },
                       succ => 1,
                       remove => [],
                       data => {},
                   },
                   updater => sub {
                       my( $outputs ) = @_;
                       my $login = $yote->domain('localhost')->get_apps->{logintest}->get__login->{foo};
                       $login_id = "$login";
                       $outputs->{ret} = "r$login_id";
                       $outputs->{data}{$login_id} = { _cls => "Yote::App::Login" };
                   },
    );
is( $sess_id, $old_sess_id, "session id same after signup" );

$sess_id = expect( test => "LoginTest fetch again two",
                   inputs => { app => "logintest",
                               sess_id => $sess_id,
                               action => "hello" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => {},
                       ret => "vHELLO WORLD",
                       data => {},
                       remove => [],
                       succ => 1,
                   },
    );
is( $sess_id, $old_sess_id, "same sess id after refetch" );

$sess_id = expect( test => "LoginTest fetch again three",
                   inputs => { app => "logintest",
                               sess_id => $sess_id,
                               args => "vTHERE",
                               action => "echo" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => {},
                       ret => "vHello THERE",
                       data => {},
                       remove => [],
                       succ => 1,
                   },
    );
is( $sess_id, $old_sess_id, "same sess id after echo" );

$sess_id = expect( test => "LoginTest fetch again four",
                   inputs => { app => "logintest",
                               sess_id => $sess_id,
                               args => "vagain",
                               action => "echo" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => {},
                       ret => "vHello again",
                       data => {},
                       remove => [],
                       succ => 1,
                   },
    );
is( $sess_id, $old_sess_id, "same sess id after echo" );

my $widget_id;
$sess_id = expect( test => "LoginTest toggle on test 1",
                   inputs => { app => "logintest",
                               sess_id => $sess_id,
                               action => "toggle" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => {
                           Widget => ['hi'],
                           LoginTest => [qw( hello fetch update signup login echo toggle )] },
                       ret => "vtoggled",
                       data => { $app_id => { _cls => "LoginTest", } },
                       remove => [],
                       succ => 1,
                   },
                   updater => sub {
                       my $outputs = shift;
                       my $widget = $yote->domain('localhost')->get_apps->{logintest}->get_toggler;
                       $widget_id = "$widget";
                       $outputs->{data}{$widget_id} = { _cls => "Widget" };
                       $outputs->{data}{$app_id}{toggler} = "r$widget_id";
                   },
    );
is( $sess_id, $old_sess_id, "same sess id after toggle" );

$sess_id = expect( test => "LoginTest hi toggle 1 allowed",
                   inputs => { app     => "logintest",
                               target  => $widget_id,
                               sess_id => $sess_id,
                               action  => "hi" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => {},
                       ret => "vhi there",
                       data => {},
                       remove => [],
                       succ => 1,
                   },
    );
is( $sess_id, $old_sess_id, "same sess id after hi" );


$sess_id = expect( test => "LoginTest toggle off test 1",
                   inputs => { app => "logintest",
                               sess_id => $sess_id,
                               action => "toggle" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => { LoginTest => [qw( hello fetch update signup login echo toggle )] },
                       ret => "vtoggled",
                       data => { $app_id => { _cls => "LoginTest" } },
                       remove => [$widget_id],
                       succ => 1,
                   },
    );
is( $sess_id, $old_sess_id, "same sess id after togle" );


$yote = init_test( <<"END" );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  'localhost':
    APPS:
      'logintest': 
        MAIN-APP-CLASS: 'LoginTest'
        APP-METHODS:
          'LoginTest':
            PUBLIC:
               - update
               - fetch
               - give_arry
               - give_hash
               - give_amix
               - give_hmix
               - array_in
               - make_widget
               - widget_set
               - widget_geta
               - widget_geth
          'Widget':
            PUBLIC:
               - hi
               - name
               - badsend
               - attachstuff
END

$app = $yote->domain('localhost')->fetch_app('logintest');
$app_id = "$app";

my $sess_old_id = expect( test => "mixy logintest",
                          inputs => { app => "logintest", action => "fetch" },
                          outputtype => 'text/json',
                          outputs => {
                              defs => { LoginTest => [qw( fetch update give_arry give_hash give_amix give_hmix array_in make_widget widget_set widget_geta widget_geth )] },
                              ret => "r$app_id",
                              data => { $app_id => { _cls => "LoginTest" } },
                              remove => [],
                              succ => 1,
                          },
                      );


$sess_id = expect( test => "mixy logintest",
                   inputs => { app => "logintest", action => "fetch" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => { 'LoginTest' => [qw( fetch update give_arry give_hash give_amix give_hmix array_in make_widget widget_set widget_geta widget_geth )] },
                       ret => "r$app_id",
                       data => { $app_id => { _cls => "LoginTest" } },
                       remove => [],
                       succ => 1,
                   },
    );
ok( $sess_id > 0, "logintest with session" );
$old_sess_id = $sess_id;

$sess_id = expect( test => "mixy give_arry",
                   inputs => { app => "logintest", action => "give_arry", sess_id => $sess_id },
                   outputtype => 'text/json',
                   outputs => {
                       defs => {},
                       ret => ['vhere are','vreturn values'],
                       data => {},
                       remove => [],
                       succ => 1,
                   },
    );
is( $sess_id, $old_sess_id, "same sess id after get_arry" );

$sess_id = expect( test => "mixy give_hash",
                      inputs => { app => "logintest", action => "give_hash", sess_id => $sess_id },
                      outputtype => 'text/json',
                      outputs => {
                          defs => {},
                          ret => { what => 'vDO YOU', want => "vTO SAY" },
                          data => {},
                          remove => [],
                          succ => 1,
                      },
    );
is( $sess_id, $old_sess_id, "same sess id after get_hash" );

$sess_id = expect( test => "mixy give_amix",
                      inputs => { app => "logintest", action => "give_amix", sess_id => $sess_id },
                      outputtype => 'text/json',
                      outputs => {
                          defs => {},
                          ret => [ { this => "vISAHASH" } ],
                          data => {},
                          remove => [],
                          succ => 1,
                      },
    );
is( $sess_id, $old_sess_id, "same sess id after get_amix" );

$sess_id = expect( test => "mixy give_amix",
                      inputs => { app => "logintest", action => "give_hmix", sess_id => $sess_id },
                      outputtype => 'text/json',
                      outputs => {
                          defs => {},
                          ret => { arry => [ map { "v$_" } (1..4) ] },
                          data => {},
                          remove => [],
                          succ => 1,
                      },
    );
is( $sess_id, $old_sess_id, "same sess id after get_amix" );

expect( test => "mixy array_in",
        inputs => { app => "logintest",
                    action => "array_in",
                    args => [ map { "v$_" } qw( this was an array ) ],
                    sess_id => $sess_id },
        outputtype => 'text/json',
        outputs => {
            defs => {},
            ret => "vthis was an array",
            data => {},
            remove => [],
            succ => 1,
        },
    );

my( $widg_one_id, $widg_two_id );
expect( test => "mixy give W 1",
        inputs => { app => "logintest",
                    action => "make_widget",
                    args => "vNameOne",
                    sess_id => $sess_id },
        outputtype => 'text/json',
        outputs => {
            defs => { Widget =>[qw( hi name badsend attachstuff )] },
            remove => [],
            succ => 1,
        },
        updater => sub {
            my( $outputs, $result ) = @_;
            ( $widg_one_id ) = ( $result->{ret} =~ /^r(.*)/ );
            $outputs->{data}{$widg_one_id} = { _cls => 'Widget', name => "vNameOne" };
            $outputs->{ret} = "r$widg_one_id";
        },
    );
ok( $widg_one_id > 0, "got a widget one" );

expect( test => "mixy give W 2",
        inputs => { app => "logintest",
                    action => "make_widget",
                    args => "vNameTwo",
                    sess_id => $sess_id },
        outputtype => 'text/json',
        outputs => {
            defs => { Widget =>[qw( hi name badsend attachstuff )] },
            remove => [],
            succ => 1,
        },
        updater => sub {
            my( $outputs, $result ) = @_;
            ( $widg_two_id ) = ( $result->{ret} =~ /^r(.*)/ );
            $outputs->{data}{$widg_two_id} = { _cls => 'Widget', name => "vNameTwo" };
            $outputs->{ret} = "r$widg_two_id";
        },
    );
ok( $widg_two_id > 0, "got a widget two" );

expect( test => "mixy give nowidg",
        inputs => { app => "logintest",
                    action => "widget_geta",
                    sess_id => $sess_id },
        outputtype => 'text/json',
        outputs => {
            defs => {},
            ret => 'u',
            remove => [],
            data => {},
            succ => 1,
        },
    );


expect( test => "mixy set widg one",
        inputs => { app => "logintest",
                    action => "widget_set",
                    args => "r$widg_one_id",
                    sess_id => $sess_id },
        outputtype => 'text/json',
        outputs => {
            defs => { 'LoginTest' => [qw( fetch update give_arry give_hash give_amix give_hmix array_in make_widget widget_set widget_geta widget_geth )] },
            ret => "r$widg_one_id",
            remove => [],
            data => { $app_id => { _cls => "LoginTest" } },
            succ => 1,
        },
    );

expect( test => "mixy widg one give name",
        inputs => { app => "logintest",
                    action => "name",
                    target => $widg_one_id,
                    args => "r$widg_one_id",
                    sess_id => $sess_id },
        outputtype => 'text/json',
        outputs => {
            defs => {},
            ret => "vNameOne",
            remove => [],
            data => {},
            succ => 1,
        },
    );

expect( test => "mixy get widg a",
        inputs => { app => "logintest",
                    action => "widget_geta",
                    sess_id => $sess_id },
        outputtype => 'text/json',
        outputs => {
            defs => {},
            ret => [ "r$widg_one_id", { widget => "r$widg_one_id" } ],
            remove => [],
            data => {},
            succ => 1,
        },
    );

expect( test => "mixy get widg h",
        inputs => { app => "logintest",
                    action => "widget_geth",
                    sess_id => $sess_id },
        outputtype => 'text/json',
        outputs => {
            defs => {},
            ret => { widget => ["r$widg_one_id"] },
            remove => [],
            data => {},
            succ => 1,
        },
    );


expect( test => "mixy get widg h",
        inputs => { app => "logintest",
                    action => "widget_geth",
                    sess_id => $sess_old_id },
        outputtype => 'text/json',
        outputs => {
            defs => { Widget =>[qw( hi name badsend attachstuff )],
                      LoginTest => [qw( fetch update give_arry give_hash give_amix give_hmix array_in make_widget widget_set widget_geta widget_geth )] },
            ret => { widget => ["r$widg_one_id"] },
            remove => [],
            data => { $widg_one_id => { _cls => 'Widget', name => 'vNameOne' }, $app_id => { _cls => 'LoginTest' } },
            succ => 1,
        },
    );



eval {
    local( *STDERR );
    my $out;
    open( STDERR, ">>", \$out );
    expect( test => "mixy fail on badsend",
            inputs => { app => "logintest",
                        action => "badsend",
                        target => $widg_one_id,
                        sess_id => $sess_id },
            outputtype => 'text/json',
            outputs => {
                ret => "failed",
                succ => 0,
            },
        );
    like( $out, qr/Got blessed non yote obj reference/, "badsend die msg" );
};

my( $hash_id, $arry_id );
expect( test => "mixy get attached stuff",
        inputs => { app => "logintest",
                    action => "attachstuff",
                    target => $widg_one_id,
                    args => 'u',
                    sess_id => $sess_id },
        outputtype => 'text/json',
        outputs => {
            defs => { Widget =>[qw( hi name badsend attachstuff )] },
            ret => "vATTACHED",
            succ => 1,
            remove => [],
            data => { $widg_one_id => { _cls => "Widget", name => "vNameOne", STUFF => "vKINDA STUFF" } },
        },
        updater => sub {
            my( $outputs, $result ) = @_;
            my $store = $yote->store;
            my $widg = $store->fetch( $widg_one_id );
            my $arry = $widg->get_arry;
            $arry_id = $store->existing_id( $arry );
            $hash_id = $store->existing_id( $arry->[1] );
            $outputs->{data}{$arry_id} = ["vONE","r$hash_id"];
            $outputs->{data}{$hash_id} = { TWO => "vTREE" };
            $outputs->{data}{$widg_one_id}{arry} = "r$arry_id";
        },
    );

expect( test => "mixy update for other session",
        inputs => { app => "logintest",
                    action => "update",
                    sess_id => $sess_old_id },
        outputtype => 'text/json',
        outputs => {
            defs => { Widget =>[qw( hi name badsend attachstuff )] },
            ret => "u",
            succ => 1,
            remove => [],
            data => { $widg_one_id => { _cls => "Widget", name => "vNameOne", arry => "r$arry_id" },
                      $arry_id => ["vONE","r$hash_id"],
                      $hash_id => { TWO => "vTREE" },
                  },
        },
    );


$yote = init_test( <<"END" );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  localhost:
    APPS:
      pagecounter:
        MAIN-APP-CLASS: 'PageCounter'
        APP-METHODS:
          PageCounter:
            PUBLIC:
              - fetch
              - signup
            ADMIN-REQUIRED:
              - update
END

$app = $yote->domain('localhost')->get_apps->{pagecounter};
$app_id = "$app";

$sess_id = expect(
    test => "fetch pagecounter for admin test",
    inputs => { app => "pagecounter",
                action => "fetch" },
    outputtype => 'text/json',
    outputs => {
        defs => { PageCounter =>[qw( fetch signup )] },
        succ => 1,
        remove => [],
        ret => "r$app_id",
        data => { $app_id => { _cls => "PageCounter" } },
    },
);

$sess_id = expect(
    test => "admin test update fail",
    inputs => { app => "pagecounter",
                sess_id => $sess_id,
                action => "update" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "failed",
    },
);

my $login;
$sess_id = expect(
    test => "admin test signup normal login",
    inputs => { app => "pagecounter",
                args => { login => "vfoo", password => "vbarbarbarbar", email => 'vfoo@localhost' },
                sess_id => $sess_id,
                action => "signup" },
    outputtype => 'text/json',
    outputs => {
        defs => { 'Yote::App::Login' => [] },
        succ => 1,
        remove => [],
        ret => "r$app_id",
        data => {},
    },
    updater => sub {
        my( $outputs ) = @_;
        $login = $yote->domain('localhost')->get_apps->{pagecounter}->get__login->{foo};
        $login_id = "$login";
        $outputs->{ret} = "r$login_id";
        $outputs->{data}{$login_id} = { _cls => "Yote::App::Login" };
    },
);

$yote = init_test( <<"END", 'keep' );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  localhost:
    APPS:
      pagecounter:
        MAIN-APP-CLASS: 'PageCounter'
        APP-METHODS:
          PageCounter:
            PUBLIC:
              - fetch
              - signup
            ADMIN-REQUIRED:
              - update
END


expect(
    test => "admin test signup dup login",
    inputs => { app => "pagecounter",
                args => { login => "vfoo", password => "vbarbarbarbar", email => 'vfoo@localhost' },
                sess_id => $sess_id,
                action => "signup" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "already have a login",
        sess_id => $sess_id,
    },
);

expect(
    test => "admin test. try to update with login and should fail",
    inputs => { app => "pagecounter",
                sess_id => $sess_id,
                action => "update" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "failed",
        sess_id => $sess_id,
    },
);

$login->set___is_admin(1);
$login->store->save;

$yote = init_test( <<"END", 'keep' );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  localhost:
    APPS:
      pagecounter:
        MAIN-APP-CLASS: 'PageCounter'
        APP-METHODS:
          PageCounter:
            PUBLIC:
              - fetch
              - signup
            ADMIN-REQUIRED:
              - update
END

expect(
    test => "admin test signup admin login",
    inputs => { app => "pagecounter",
                sess_id => $sess_id,
                action => "update" },
    outputtype => 'text/json',
    outputs => {
        defs => { 'Yote::App::Login' => [] },
        succ => 1,
        remove => [],
        ret => "u",
        data => { $login_id => { _cls => "Yote::App::Login" } },
    },
);


$sess_id = expect(
    test => "fetch pagecounter for admin test fetch 2",
    inputs => { app => "pagecounter",
                action => "fetch" },
    outputtype => 'text/json',
    outputs => {
        defs => { PageCounter =>[qw( fetch signup )] },
        succ => 1,
        remove => [],
        ret => "r$app_id",
        data => { $app_id => { _cls => "PageCounter" } },
    },
);

expect(
    test => "admin test signup admin login",
    inputs => { app => "pagecountery",
                sess_id => $sess_id,
                action => "update" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "failed",
        sess_id => $sess_id,
    },
);

expect(
    test => "admin test signup admin no action",
    inputs => { app => "pagecounter",
                sess_id => $sess_id,
            },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "failed",
        sess_id => $sess_id,
    },
);

expect(
    test => "admin test invalid target",
    inputs => { app => "pagecounter",
                sess_id => $sess_id,
                target => "33",
                action => 'fetch',
            },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "failed",
        sess_id => $sess_id,
    },
);

expect(
    test => "admin test signup pw too short",
    inputs => { app => "pagecounter",
                args => { login => "vfoov", password => "vbar", email => 'vfoov@localhost' },
                sess_id => $sess_id,
                action => "signup" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "Password too short",
        sess_id => $sess_id,
    },
);

expect(
    test => "admin test login taken",
    inputs => { app => "pagecounter",
                sess_id => $sess_id,
                args => { login => "vfoo", password => "vbaroooooooo", email => 'vfoov@localhost' },
                action => "signup" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "login or email already taken",
        sess_id => $sess_id,
    },
);

expect(
    test => "admin test signup email taken",
    inputs => { app => "pagecounter",
                sess_id => $sess_id,
                args => { login => "vfoov", password => "vbaroooooooo", email => 'vfoo@localhost' },
                action => "signup" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "login or email already taken",
        sess_id => $sess_id,
    },
);

expect(
    test => "admin test signup no login",
    inputs => { app => "pagecounter",
                sess_id => $sess_id,
                args => { login => "v", password => "vbaroooooooo", email => 'vfoov@localhost' },
                action => "signup" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "login too short",
        sess_id => $sess_id,
    },
);

expect(
    test => "admin test signup weird email",
    inputs => { app => "pagecounter",
                sess_id => $sess_id,
                args => { login => "vfoov", password => "vbaroooooooo", email => 'vfoolocalhost' },
                action => "signup" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "invalid looking email",
        sess_id => $sess_id,
    },
);

$yote = init_test( <<"END", 'keep' );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  localhost:
    APPS:
      pagecounter:
        MAIN-APP-CLASS: 'PageCounter'
        APP-METHODS:
          PageCounter:
            PUBLIC:
              - fetch
              - signup
              - login
              - reset_password
              - send_reset_request
              - activate_reset
              - make_reset_link
            LOGIN-REQUIRED:
              - logout
            ADMIN-REQUIRED:
              - update
END
$yote->store->save;
expect(
    test => "admin test logn wrong pw",
    inputs => { app => "pagecounter",
                args => { user => "vfoo", password => "vbarbarbarbaru" },
                sess_id => $sess_id,
                action => "login" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "login failed",
        sess_id => $sess_id,
    }
);

expect(
    test => "admin test logn wrong user",
    inputs => { app => "pagecounter",
                args => { user => "vfooo", password => "vbarbarbarbar" },
                sess_id => $sess_id,
                action => "login" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "login failed",
        sess_id => $sess_id,
    }
);


expect(
    test => "admin test signup normal login",
    inputs => { app => "pagecounter",
                args => { user => "vfoo", password => "vbarbarbarbar" },
                sess_id => $sess_id,
                action => "login" },
    outputtype => 'text/json',
    outputs => {
        defs => { 'Yote::App::Login' => [] },
        succ => 1,
        remove => [],
        ret => "r$login_id",
        sess_id => $sess_id,
        data => { $login_id => { _cls => "Yote::App::Login" } },
    },
    );

expect(
    test => "admin test signup admin login, still is_admin",
    inputs => { app => "pagecounter",
                sess_id => $sess_id,
                action => "update" },
    outputtype => 'text/json',
    outputs => {
        defs => {},
        succ => 1,
        remove => [],
        ret => "u",
        sess_id => $sess_id,
        data => {},
    },
);

expect(
    test => "admin test signup admin logout",
    inputs => { app => "pagecounter",
                sess_id => $sess_id,
                action => "logout" },
    outputtype => 'text/json',
    outputs => {
        defs => {},
        succ => 1,
        remove => [],
        ret => "vlogged out",
        data => {},
    },
);

expect(
    test => "admin test ",
    inputs => { app => "pagecounter",
                sess_id => $sess_id,
                target => "33",
                action => 'fetch',
            },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "failed",
        sess_id => $sess_id,
    },
);

$sess_id = expect(
    test => "admin test signup normal login",
    inputs => { app => "pagecounter",
                args => { user => "vfoo", password => "vbarbarbarbar" },
                sess_id => $sess_id,
                action => "login" },
    outputtype => 'text/json',
    outputs => {
        defs => { 'Yote::App::Login' => [] },
        succ => 1,
        remove => [],
        ret => "r$login_id",
        data => { $login_id => { _cls => "Yote::App::Login" } },
    },
);
$old_sess_id = $sess_id;
$sess_id = expect(
    test => "admin test signup admin logout 2",
    inputs => { app => "pagecounter",
                sess_id => $sess_id,
                action => "logout" },
    outputtype => 'text/json',
    outputs => {
        defs => {},
        succ => 1,
        remove => [],
        ret => "vlogged out",
        data => {},
    },
    );
is( $sess_id, 0, 'session id zerod out' );
isnt( $sess_id, $old_sess_id, 'session expired after logout' );

$sess_id = expect(
    test => "admin test signup normal login",
    inputs => { app => "pagecounter",
                args => { user => "vfoo\@localhost", password => "vbarbarbarbar" },
                sess_id => $sess_id,
                action => "login" },
    outputtype => 'text/json',
    outputs => {
        defs => { 'Yote::App::Login' => [] },
        succ => 1,
        remove => [],
        ret => "r$login_id",
        data => { $login_id => { _cls => "Yote::App::Login" } },
    },
);

$sess_id = expect(
    test => "admin test signup reset password too short",
    inputs => { app => "pagecounter",
                args => { oldpassword => "vbarbarbarbar",
                          newpassword => "vfoofoo" },
                sess_id => $sess_id,
                action => "reset_password" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "Password too short",
    },
);

$sess_id = expect(
    test => "admin test signup reset password",
    inputs => { app => "pagecounter",
                args => { oldpassword => "vbarbarbarbar",
                          newpassword => "vfoofoofoofoo" },
                sess_id => $sess_id,
                action => "reset_password" },
    outputtype => 'text/json',
    outputs => {
        defs => { 'Yote::App::Login' => [] },
        succ => 1,
        remove => [],
        ret => "vReset Password",
        data => { $login_id => { _cls => "Yote::App::Login" } },
    },
);

$sess_id = expect(
    test => "admin test signup reset password wrong old",
    inputs => { app => "pagecounter",
                args => { oldpassword => "vbarbarbarbar",
                          newpassword => "vfoofoofoofoo" },
                sess_id => $sess_id,
                action => "reset_password" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "wrong password",
    },
);


$sess_id = expect(
    test => "admin test signup admin logout 3",
    inputs => { app => "pagecounter",
                sess_id => $sess_id,
                action => "logout" },
    outputtype => 'text/json',
    outputs => {
        defs => {},
        succ => 1,
        remove => [],
        ret => "vlogged out",
        data => {},
    },
    );
is( $sess_id, 0, 'session id zerod out' );
isnt( $sess_id, $old_sess_id, 'session expired after logout' );

$sess_id = expect(
    test => "admin test signup login with old password ",
    inputs => { app => "pagecounter",
                args => { user => "vfoo\@localhost", password => "vbarbarbarbar" },
                sess_id => $sess_id,
                action => "login" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "login failed",
    },
);


$sess_id = expect(
    test => "admin test login with new password 1",
    inputs => { app => "pagecounter",
                args => { user => "vfoo\@localhost", password => "vfoofoofoofoo" },
                sess_id => $sess_id,
                action => "login" },
    outputtype => 'text/json',
    outputs => {
        defs => { 'Yote::App::Login' => [] },
        succ => 1,
        remove => [],
        ret => "r$login_id",
        data => { $login_id => { _cls => "Yote::App::Login" } },
    },
);

#no op login again, no new data
$sess_id = expect(
    test => "admin test login with new password 2",
    inputs => { app => "pagecounter",
                args => { user => "vfoo\@localhost", password => "vfoofoofoofoo" },
                sess_id => $sess_id,
                action => "login" },
    outputtype => 'text/json',
    outputs => {
        defs => {},
        succ => 1,
        remove => [],
        ret => "r$login_id",
        data => {},
    },
);

my $reset_tok;
$sess_id = expect(
    test => "make reset request noemail, still makes (nonfunctioning) link",
    inputs => { app => "pagecounter",
                action => "make_reset_link",
                args => "vnothere\@localhost" },
    outputtype => 'text/json',
    outputs => {
        succ => 1,
        defs => {},
        remove => [],
        data => {},
    },
    updater => sub {
        my( $outputs, $result ) = @_;
        if( $result->{ret} =~ m~^vhttps://localhost/pagecounter#recover;tok=(\d+)~ ) {
            $reset_tok = $1;
            $outputs->{ret} = $result->{ret};
        }
    },
    );

$sess_id = expect(
    test => "activate reset request bogus token",
    inputs => { app => "pagecounter",
                action => "activate_reset",
                args => "v$reset_tok" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "bad or expired token",        
    },
);



$sess_id = expect(
    test => "make reset request",
    inputs => { app => "pagecounter",
                action => "make_reset_link",
                args => "vfoo\@localhost" },
    outputtype => 'text/json',
    outputs => {
        succ => 1,
        defs => {},
        remove => [],
        data => {},
    },
    updater => sub {
        my( $outputs, $result ) = @_;
        if( $result->{ret} =~ m~^vhttps://localhost/pagecounter#recover;tok=(\d+)~ ) {
            $reset_tok = $1;
            $outputs->{ret} = $result->{ret};
        }
    },
    );

$sess_id = expect(
    test => "activate reset request no token",
    inputs => { app => "pagecounter",
                action => "activate_reset",
                args => "v" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "bad or expired token",        
    },
);

$login->set___reset_token_good_until( time - 1 );
$login->store->save;

$sess_id = expect(
    test => "activate reset request wrong time",
    inputs => { app => "pagecounter",
                action => "activate_reset",
                args => "v$reset_tok" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "bad or expired token",        
    },
);

$login->set___reset_token_good_until( time + 100 );
$login->store->save;

# ------ token is now destroyed,

$sess_id = expect(
    test => "activate reset request wrong time",
    inputs => { app => "pagecounter",
                action => "activate_reset",
                args => "v$reset_tok" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "bad or expired token",        
    },
);



# ------ redo token
my $old_tok = $reset_tok;
expect(
    test => "make reset request redo",
    inputs => { app => "pagecounter",
                action => "make_reset_link",
                args => "vfoo\@localhost" },
    outputtype => 'text/json',
    outputs => {
        succ => 1,
        defs => {},
        remove => [],
        data => {},
    },
    updater => sub {
        my( $outputs, $result ) = @_;
        if( $result->{ret} =~ m~^vhttps://localhost/pagecounter#recover;tok=(\d+)~ ) {
            $reset_tok = $1;
            $outputs->{ret} = $result->{ret};
        }
    },
    );

expect(
    test => "make reset request redo newer tok",
    inputs => { app => "pagecounter",
                action => "make_reset_link",
                args => "vfoo\@localhost" },
    outputtype => 'text/json',
    outputs => {
        succ => 1,
        defs => {},
        remove => [],
        data => {},
    },
    updater => sub {
        my( $outputs, $result ) = @_;
        if( $result->{ret} =~ m~^vhttps://localhost/pagecounter#recover;tok=(\d+)~ ) {
            $reset_tok = $1;
            $outputs->{ret} = $result->{ret};
        }
    },
    );


$sess_id = expect(
    test => "activate reset request old token",
    inputs => { app => "pagecounter",
                action => "activate_reset",
                args => "v$old_tok" },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "bad or expired token",        
    },
);


$sess_id = expect(
    test => "activate reset request with new token",
    inputs => { app => "pagecounter",
                action => "activate_reset",
                args => "v$reset_tok" },
    outputtype => 'text/json',
    outputs => {
        succ => 1,
        defs => { 'Yote::App::Login' => [] },
        remove => [],
        ret => "r$login_id",        
        data => { $login_id => { _cls => "Yote::App::Login" } },
    },
);

expect(
    test => "reset password no session given",
    inputs => {
        app => "pagecounter",
        action => "reset_password",
        args => { newpassword => "vbazbazbaz", token => "v$reset_tok" }
    },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "bad or expired token",
    },
);


expect(
    test => "reset password no session, no token given",
    inputs => {
        app => "pagecounter",
        action => "reset_password",
        args => { newpassword => "vbazbazbaz" }
    },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "failed",
    },
);


expect(
    test => "reset password wrong token given",
    inputs => {
        app => "pagecounter",
        action => "reset_password",
        sess_id => $sess_id,
        args => { newpassword => "vbazbazbaz", token => "v".(1+$reset_tok) }
    },
    outputtype => 'text/json',
    outputs => {
        succ => 0,
        ret => "bad or expired token",
    },
);

expect(
    test => "reset password session given",
    inputs => {
        app => "pagecounter",
        action => "reset_password",
        sess_id => $sess_id,
        args => { newpassword => "vbazbazbaz", token => "v$reset_tok" }
    },
    outputtype => 'text/json',
    outputs => {
        defs => { 'Yote::App::Login' => [] },
        succ => 1,
        remove => [],
        ret => "vReset Password",
        data => { $login_id => { _cls => "Yote::App::Login" } },
    },
);



done_testing;

__END__
