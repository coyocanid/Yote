#!/usr/bin/perl
use strict;
use warnings;

use Carp;
use File::Temp qw/ :mktemp tempdir /;
use JSON;
use MIME::Lite;
use Test::More;

use lib '/home/wolf/open_source/recordstore/lib';
use lib '/home/wolf/open_source/objectstore/lib';
use lib './lib';
use lib 't/lib';

use Yote;
use Yote::App::Endpoint;

$SIG{ __DIE__ } = sub { Carp::confess( @_ ) };


my( $dir, $yote, $sess_id );
sub init_test {
    my( $yaml, $keepdir ) = @_;
    unless( $keepdir ) {
        $dir = tempdir( CLEANUP => 1 );
    }
    $yaml =~ s!TMPDIR!$dir/data_store!gs;
    $ENV{YOTE_ROOT_DIRECTORY} = $dir;
    open my $out, '>', "$dir/yote-conf.yaml";
    print $out "---\n$yaml\n";
    close $out;
    $yote = Yote::App::Endpoint->init( $dir );
    return $yote;
}

sub expect {
    my( %args ) = @_;
    my( $test, $inputs, $outputtype, $outputs, $sess_id, $updater ) = @args{ 'test', 'inputs', 'outputtype', 'outputs', 'sess_id', 'updater' };
    my( $outt, $data ) = Yote::App::Endpoint->handle( { payload => to_json( $inputs ) }, 'localhost' );
    my $result = from_json($data);
    $result->{sess_id} = substr( $result->{sess_id}, 1 );
#    print STDERR " SEND <---- ".to_json($inputs)."\nGOT --> $data\n\n";
    my $new_sess_id = $result->{sess_id};
    if( $updater ) {
        $updater->( $outputs, $result );
    }
#    print STDERR Data::Dumper->Dump([$outputs]);
    if( $outputs->{defs} ) {
        for my $cls (keys %{$outputs->{defs}}) {
            $outputs->{defs}{$cls} = [ sort @{$outputs->{defs}{$cls}} ];
        }
    }
    if( $outputs->{remove} ) {
        $outputs->{remove} = [ sort @{$outputs->{remove}} ];
    }
    if( $result->{defs} ) {
        for my $cls (keys %{$result->{defs}}) {
            $result->{defs}{$cls} = [ sort @{$result->{defs}{$cls}} ];
        }
    }
    if( ! $outputs->{sess_id} ) {
        delete $result->{sess_id};
    }
    if( $outputtype ) {
#        is( $outt, $outputtype, "output type of $test" );
    }
    is_deeply( $result, $outputs, "output of $test" );
    return $new_sess_id;
} #expect

$yote = init_test( <<"END" );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  'localhost':
    APPS:
      'logintest': 
        MAIN-APP-CLASS: 'LoginTest'
        APP-METHODS:
          'LoginTest':
            PUBLIC:
               - signup
               - send_reset_request
               - activate_reset
               - fetch
END

my $app = $yote->domain('localhost')->get_apps->{logintest};
my $app_id = "$app";

$sess_id = expect( test => "LoginTest fetch",
                   inputs => { app     => "logintest",
                               action  => "fetch" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => { 'LoginTest' => [qw(activate_reset fetch send_reset_request signup)] },
                       succ => 1,
                       ret    => "r$app_id",
                       remove => [],
                       data => { $app_id => { _cls => 'LoginTest' } },
                   },
    );


my( $login, $login_id );
expect( test => "LoginTest signup",
        inputs => { app => "logintest",
                    sess_id => $sess_id,
                    args => { login => "vwolf",
                              password => "vwolfwolfwolf",
                              email => 'vwolf@localhost' },
                    action => "signup" },
        outputtype => 'text/json',
        outputs => {
            defs => { 'Yote::App::Login' => [] },
            succ => 1,
            remove => [],
            data => {},
        },
        updater => sub {
            my( $outputs ) = @_;
            $login = $yote->domain('localhost')->get_apps->{logintest}->get__login->{wolf};
            $login_id = "$login";
            $outputs->{ret} = "r$login_id";
            $outputs->{data}{$login_id} = { _cls => "Yote::App::Login" };
        },
    );

$sess_id = expect( test => "LoginTest fetch",
                   inputs => { app     => "logintest",
                               action  => "fetch" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => { 'LoginTest' => [qw(activate_reset fetch send_reset_request signup)] },
                       succ => 1,
                       ret    => "r$app_id",
                       remove => [],
                       data => { $app_id => { _cls => 'LoginTest' } },
                   },
    );

$sess_id = expect( test => "send reset request bad email",
                   inputs => { app => "logintest",
                               args => 'vwolfu@localhost',
                               sess_id => $sess_id,
                               action => "send_reset_request" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => { 'LoginTest' => [qw(activate_reset fetch send_reset_request signup)] },
                       ret => "vsent",
                       data => { $app_id => { _cls => "LoginTest" } },
                       remove => [],
                       succ => 1,
                   },
    );


MIME::Lite->send( 'sendmail', "t/script/fakemail $dir" );

my $reset_tok;
$sess_id = expect( test => "send reset request",
                   inputs => { app => "logintest",
                               args => 'vwolf@localhost',
                               sess_id => $sess_id,
                               action => "send_reset_request" },
                   outputtype => 'text/json',
                   outputs => {
                       defs => {},# 'LoginTest' => [qw(activate_reset fetch send_reset_request signup)] },
                       ret => "vsent",
                       data => {},# $app_id => { _cls => "LoginTest" } },
                       remove => [],
                       succ => 1,
                   },
    );

open my $in, '<', "$dir/fakemail";
my $mail = '';
while( <$in> ) {
    $mail .= $_;
}
close $in;

if( $mail =~ m~text/plain.*?https://localhost/logintest#recover;tok=(\d+)~s ) {
    $reset_tok = $1;
}

$sess_id = expect(
    test => "activate reset request with new token",
    inputs => { app => "logintest",
                action => "activate_reset",
                args => "v$reset_tok" },
    outputtype => 'text/json',
    outputs => {
        succ => 1,
        defs => { 'Yote::App::Login' => [] },
        remove => [],
        ret => "r$login_id",        
        data => { $login_id => { _cls => "Yote::App::Login" } },
    },
);

done_testing;
