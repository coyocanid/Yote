#!/usr/bin/perl

use strict;
use warnings;
no warnings 'uninitialized';

use CGI;
use Data::Dumper;

use lib "/opt/yote/lib";

use yote_config;
use Yote::Endpoint;

my $q    = CGI->new;
my $vars = $q->Vars;

# --------- find files that are inputted
my @files = $vars->{files};
my $file_idx = 0;
    print STDERR ")$file_idx of @files\n";
my $get_files = sub {
    print STDERR ")$file_idx of @files\n";
    return unless @files && $file_idx < @files;
    my $fh = $q->upload("file_$file_idx");
    $file_idx++;
    my $fn = "$fh";
    my $tmpfn = $q->tmpFileName( $fh );
    return { tempname => $tmpfn,
	     handle   => $fh,
	     name     => $fn };
};

$vars = {%$vars};
$vars->{uploads} = $get_files;

my $endpoint = Yote::Endpoint->load( $yote_config::config );
unless( $endpoint ) {
    print STDERR "ERROR LOADING ENDPOINT : $@\n";
}

#print STDERR "Handle?\n";
my( $content_type, $content ) = $endpoint->handle( $vars );
#print STDERR "Handled ($content)\n";

print "Content-Type: $content_type\nContent-Length: ".length($content)."\n\n$content";

__END__

ipcs -m | perl -e 'use IPC::SharedMem;;<>;<>;while(<>){ ($key,$smid,$owner) = split( /\s+/, $_ ); next unless $owner eq "wolf";print "$owner,$key\n";$shm=IPC::SharedMem->new($key, 8, S_IRWXU); $shm->attach;$shm->remove;$shm->detach }'


------ Shared Memory Segments --------
key        shmid      owner      perms      bytes      nattch     status      
0x0052e2c1 0          postgres   600        56         6                       
0x01010142 1          root       700        8          7                       
0x02010142 2          root       700        176        7                       
0x00000000 196613     wolf       600        524288     2          dest         
0x00000000 9          wolf       600        1048576    2          dest         
0x00000000 10         wolf       600        4194304    2          dest         
0x00000000 11         wolf       600        67108864   2          dest         
0x00000000 163858     wolf       600        393216     2          dest         
0x00000000 360469     www-data   700        8          0                       
0x00000000 360470     www-data   700        176        0                       
0x00000000 41         wolf       600        4194304    2          dest         
0x00000000 196651     wolf       606        4765032    2          dest         
0x00000000 196652     wolf       606        4765032    2          dest         
0x00000000 163892     wolf       606        11704320   2          dest         
0x00000000 163893     wolf       606        11704320   2          dest         
